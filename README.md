# PROJECT DESCRIPTION
This repository contains docker files for getting the hdf5 rest server and the
hdf5 web gui up and running as painlessly as possible.  It contains the
following files and directories:
```bash
    hdf5-rest-server/
    ├── config.py*
    ├── Dockerfile
    └── requirements.txt
    hdf5-web-gui/
    ├── config.json
    ├── Dockerfile
    ├── index.html
    └── nginx.conf
    docker-compose.yml
```

The two directories contain Dockerfile files for creating docker images, as
well as configuration files, some of which will need to be altered for each
running environment.

The docker-compose.yml file contains commands necessary to run everything.

![screenshot](screenshot.png)

# DEMO
A demo installation can be seen here:
https://demo.maxiv.lu.se/hdf5-web-gui/html/

# SETUP AND RUN
Running this application requires docker and docker-compose.

## DOCKER & DOCKER-COMPOSE INSTALL

### DEBIAN
Followed instructions here:
https://docs.docker.com/engine/installation/linux/docker-ce/debian/

For my setup, did the following to install docker:
```bash
sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce
sudo usermod -a -G docker $USER
sudo curl -L https://github.com/docker/compose/releases/download/1.17.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod 755 /usr/local/bin/docker-compose
sudo systemctl start docker
```

### CENTOS7
Followed instructions here:
https://docs.docker.com/engine/installation/linux/docker-ce/centos/

```bash
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce
sudo usermod -a -G docker $USER
sudo curl -L https://github.com/docker/compose/releases/download/1.17.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod 755 /usr/local/bin/docker-compose
sudo systemctl start docker
```

## GET APPLICATION CODE, INSTALL, RUN

### GIT THE CODE
```bash
git clone git@gitlab.maxiv.lu.se:web-gui-applications/hdf5-web-docker.git
cd hdf5-web-docker/
```

### CONFIGURE
The following four config files need to be altered in the necessary places:
```bash
    hdf5-web-gui/config.json
        "hdf5DataServer": "https://w-jasbru-pc-0.maxiv.lu.se:6050",
        "serviceUrl": "https://w-jasbru-pc-0.maxiv.lu.se:8443/hdf5-web-gui/html/app.html"

    hdf5-web-gui/nginx.conf
        server_name w-jasbru-pc-0.maxiv.lu.se;

    hdf5-rest-server/config.py
        # The server domain on which h5serv is running
        'domain':  'w-jasbru-pc-0.maxiv.lu.se',
        # The domain to which h5serv will allow CORS
        'cors_domain': 'https://w-jasbru-pc-0.maxiv.lu.se:8443',

    docker-compose.yml
        # The ssl certificates for the hdf5 server
        - /etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.crt:/etc/ssl/certs/h5serv-cert.crt:ro
        - /etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.key:/etc/ssl/certs/h5serv-cert.key:ro
        # The ssl certificates for the nginx server
        - /etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.crt:/etc/ssl/certs/h5serv-cert.crt:ro
        - /etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.key:/etc/ssl/certs/h5serv-cert.key:ro
        # Set the host name - needs to match certificate and config file contents
        hostname: w-jasbru-pc-0.maxiv.lu.se
```

### INSTALL AND RUN
Download the necessary repositories, create the docker images,  set things up,
and run it all in fell swoop (takes about a few minutes the first time):
```bash
docker-compose up -d
```

Alternatively, the docker services may be run on separate machines, as long as
the config file are properly setup:
machine:
```bash
docker-compose up -d hdf5-rest-server
```
```bash
docker-compose up -d hdf5-web-gui
```

### MONITOR LOGS
Monitor the logs:
```bash
docker-compose logs -f
```

### USE HDF5 WEB GUI
https://my-web-server:8443

### VIEW H5SERV RAW OUTPUT
https://my-web-server:6050


# DOCKER CONTAINER & IMAGE CONTROL

Some basic docker stuff:

Stop the application containers:
```bash
cd web-status-docker/
docker-compose down
```

View all images:
```bash
docker image ls
```

View all running containers
```bash
docker ps
```
